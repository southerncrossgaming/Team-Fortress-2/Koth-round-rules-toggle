#pragma semicolon 1

#include <sourcemod>
#include <tf2>
#include <tf2_stocks>

new Handle:g_match_end = INVALID_HANDLE;
new Handle:g_toggleRound;

#define PLUGIN_VERSION		"1.0"

public Plugin:myinfo = {
	name		= "[TF2] KOTH Capture Toggle",
	author		= "Rowedahelicon",
	description	= "Toggle of the KOTH round timer",
	version		= PLUGIN_VERSION,
	url			= "http://www.rowedahelicon.com"
};


public OnPluginStart()
{
	if(!HookEvent("teamplay_point_captured", EventPointCaptured))
	{
		SetFailState("Could not hook the teampoint_point_captured event.");
	}

	if(!HookEvent("teamplay_round_start", EventStart))
	{
        SetFailState("Could not hook the teamplay_round_start event.");
	}
	g_match_end = FindConVar("mp_match_end_at_timelimit");
	g_toggleRound = CreateConVar("koth_round_toggle", "1", "Adjusts this plugin for a koth map");
}

public EventPointCaptured(Handle:event,const String:name[],bool:dontBroadcast)
{
	new round_toggle;
	round_toggle = GetConVarInt(g_toggleRound);
	if(round_toggle >= 1){
		SetConVarBool(g_match_end, false);
	}
}

public EventStart(Handle:event,const String:name[],bool:dontBroadcast)
{
	new round_toggle;
	round_toggle = GetConVarInt(g_toggleRound);
	if(round_toggle >= 1){
		SetConVarBool(g_match_end, true);
	}
}